<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ViewsController extends Controller
{
    //

    public function viewLogin(){
        return view('login.login');
    }

    public function viewPanel(){
        return view('panel.index');
    }

    public function viewProducto(){
        return view('producto.index');
    }
    public function viewCrearProducto(){
        return view('producto.create');
    }

    public function viewUsuario(){
        return view('usuario.index');
    }

    public function viewCrearUsuario(){
        return view('usuario.create');
    }
}
