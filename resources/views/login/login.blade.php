@extends('layout')


@section('content')



    @if ($errors->any())
        <div class="alert alert-danger">
            <strong></strong> <br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <br>

    <div class="card">
        <div class="card-header">
          Ingresar
        </div>
        <div class="card-body">
          
            <form>
                @csrf
        
        
                 <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>RUT:</strong>
                            <input type="text" name="rut" id="rut" class="form-control" placeholder="RUT">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Contraseña:</strong>
                            <input type="password" class="form-control" name="password"  id="password" placeholder="Contraseña">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                        <button type="submit" onclick="loginUsuario();" class="btn btn-primary">Ingresar</button>
                        
                    </div>
                </div>
        
        
            </form>

        </div>
      </div>


    




    

    <br>


    

<!--
<center><a href="{{('/nuevo/usuario')}}"><button type="submit" class="btn btn-success">Registrar</button></a></center>
!-->


    
    


@endsection