@extends('layout')


@section('content')



    @if ($errors->any())
        <div class="alert alert-danger">
            <strong></strong> <br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <br>

    <div class="card">
        <div class="card-header">
          Módulo Desafío
        </div>
        <div class="card-body">
          
            <a href="{{('/usuario/crear')}}"><button type="button" class="btn btn-primary btn-lg btn-block">USUARIOS</button></a>
            <br>
            <a href="{{('/producto/crear')}}"><button type="button" class="btn btn-warning btn-lg btn-block">PRODUCTOS</button></a>

        </div>
      </div>


    
    


@endsection