@extends('layout')


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Nuevo Producto</h2>
            </div>
            <div class="pull-right">
               
            </div>
        </div>
    </div>


    @if ($errors->any())
        <div class="alert alert-danger">
            <strong></strong> <br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <form>
    	@csrf


         <div class="row">
		    <div class="col-xs-12 col-sm-12 col-md-12">
		        <div class="form-group">
		            <strong>Nombre:</strong>
		            <input type="text" name="nombre" id="nombre" class="form-control" placeholder="Nombre">
		        </div>
		    </div>
		    <div class="col-xs-12 col-sm-12 col-md-12">
		        <div class="form-group">
		            <strong>Categoria:</strong>
		            <textarea class="form-control" style="height:150px" name="categoria" id="categoria" placeholder="Categoria"></textarea>
		        </div>
            </div>
            
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
            
		    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
		            <button onclick="RegistrarProducto();" class="btn btn-primary">Guardar</button>
		    </div>
		</div>


    </form>


@endsection