@extends('layout')


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Productos</h2>
            </div>
            <div class="pull-right">
            
                <a class="btn btn-success" href="{{ ('/producto/crear') }}"> Crear Nuevo Producto</a>
                
                
            </div>
        </div>
    </div>


    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif



    <center><a class="btn btn-info" onclick="ExportarProducto();"> Exportar Productos</a></center>


@endsection