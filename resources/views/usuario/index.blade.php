@extends('layout')


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Productos</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('producto.create') }}"> Crear Nuevo Producto</a>
                
            </div>
        </div>
    </div>


    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif


    <table class="table table-bordered">
        <tr>
            <th>N°</th>
            <th>Nombre</th>
            <th>Categoria</th>
            <th width="280px">Acción</th>
        </tr>
	    @foreach ($producto as $prod)
	    <tr>
	        <td>{{ ++$i }}</td>
	        <td>{{ $prod->nombre }}</td>
	        <td>{{ $prod->categoria }}</td>
	        <td>
                <form action="{{ route('producto.destroy',$prod->id) }}" method="POST">
                    <a class="btn btn-info" href="{{ route('producto.show',$prod->id) }}">Ver</a>
                    <a class="btn btn-primary" href="{{ route('producto.edit',$prod->id) }}">Modificar</a>
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger">Eliminar</button>
                </form>
	        </td>
	    </tr>
	    @endforeach
    </table>

    <center><a class="btn btn-info" href="{{ ('/exportar') }}"> Exportar Productos</a></center>


@endsection