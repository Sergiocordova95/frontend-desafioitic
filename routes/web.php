<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('login.login');
});


//Route::resource('producto','ProductoController');
//Route::get('/exportar', 'ProductoController@exportarProductos');

Route::get('/panel', 'ViewsController@viewPanel');
Route::get('/producto', 'ViewsController@viewProducto');
Route::get('/usuario','ViewsController@viewUsuario');

Route::get('/producto/crear','ViewsController@viewCrearProducto');

Route::get('/usuario/crear','ViewsController@viewCrearUsuario');

